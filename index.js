const fetch = require("node-fetch");
let ciudades = ["Paris,fr", "Berlin,de", "Beijing,cn", "Vilnius,lt", "Lisboa,pt", "Madrid,es", "Barcelona,es", "Nantes,fr", "Amsterdam,nl", "Rotterdam,nl"];

function main() {

    for (let i = 0; i < 3; i++) {
        let ciudad = ciudades[Math.floor(Math.random() * ciudades.length)];
        let url = `http://api.openweathermap.org/data/2.5/weather?q=${ciudad}&APPID=d25248633be3ce6dfafae07530ea5b98&units=metric`;
        fetch(url)
            .then(response => response.json())
            .then(data => {
                console.log(ciudad)
                console.log(data.main.temp)
            })
            .catch(error => {
                console.log("Ha ocurrido un error")
                console.log(error)
            })
    }
}
main();